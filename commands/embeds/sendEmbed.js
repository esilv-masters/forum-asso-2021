const BaseCommand = require('../../utils/structures/BaseCommand');
const {
    readFile,
    downloadFile,
    sleep
} = require('../../utils/functions/utilitaryFunctions');
const Discord = require('discord.js')


module.exports = class EmbedRolesBefore extends BaseCommand {
    constructor () {
        super('sendembed', 'embeds', [], {
            usage: 'sendembed',
            description: 'sendembed',
            categoryDisplayName: `📰 Embeds`,
            userPermissions: ['ADMINISTRATOR'],
            clientPermissions: [],
            examples: [],
            serverOnly: true,
            admin: false,
            subCommands: true
        });
    }

    async run (bot, message, args) {
        let jsonFile = message.attachments.first()

    
        const url = jsonFile.attachment
        const path = `./tmp/embeds.json`

        


        await downloadFile(path, url);
        let data = await readFile(path);
        let embedData = JSON.parse(data)

        if (embedData && (embedData.title || embedData.description)) {
            return sendEmbed(message.channel, embedData)
        }
        if (!Array.isArray(embedData) && embedData.embeds) {
            embedData = embedData.embeds
        }

        if (Array.isArray(embedData)) {
            for (let embed of embedData) {
                sendEmbed(message.channel, embed)
            }
        }

        message.delete()
    }
}

const sendEmbed = async (channel, embedData) => {
    try {
        let embed = new Discord.MessageEmbed(embedData)
        channel.send(embed)
    } catch (err) {
        if (err) channel.send(`**❌ | **INTERNAL SERVER ERRROR`).then(async (sentMessage) => {
            await sleep(5000);
            sentMessage.delete();
        })
    }
}