const BaseCommand = require('../../utils/structures/BaseCommand')
const Discord = require('discord.js');
const mongoose = require('mongoose')
const TicketArchive = require('../../src/schemas/TicketArchiveSchema')

module.exports = class TicketCloseCommand extends BaseCommand {
    constructor() {
        super('ticket-close', 'tickets', [], {
            usage: 'ticket close <optional topic>',
            description: `Ferme le ticket et le marque comme résolu.`,
            categoryDisplayName: `🎫 Tickets`,
            userPermissions: ['MANAGE_ROLES'],
            clientPermissions: [],
            examples: [],
            serverOnly: true,
            admin: false,
            subCommands: false
        });
    }

    async run(client, message, args) {
        const existingDBTicket = await mongoose.model('Ticket').findOne({ linkedChannelId: message.channel.id })
        if (existingDBTicket && existingDBTicket.id) {
            let topic = args[2] || undefined;
            TicketArchive.create({
                dmChannelId: existingDBTicket.dmChannelId,
                name: existingDBTicket.name,
                userId: existingDBTicket.userId,
                guildId: existingDBTicket.guildId,
                claimedByUserId: existingDBTicket.claimedByUserId || null,
                messages: existingDBTicket.messages,
                topic: topic || null
            }, async (err) => {
                if (err) throw err && message.chanel.send('**❌ | **Une erreur est parvenue dans la suppression de ce ticket !')
                else {
                    let dmUser = message.guild.members.cache.get(existingDBTicket.userId);
                    dmUser.createDM().then(dmChannel => {
                        dmChannel.send(`**✅ | **Votre problème a été marqué comme résolu par \`\`${message.author.username}\`\`, si ce n'est pas le cas veuillez simplement renvoyer un message ci dessous !`)
                    })
                    await mongoose.model('Ticket').deleteOne({ linkedChannelId: message.channel.id })
                    let deleteEmbed = new Discord.MessageEmbed()
                        .setDescription("Suppression du ticket dans 5 secondes...")
                        .setColor('ff5733')
                    message.channel.send(deleteEmbed);
                    await sleep(5000);
                    message.channel.delete();
                }
            })
        } else {
            message.channel.send(`**❌ | **Cette commande peut uniquement être utilisée dans un ticket !`)
        }
    }
}

const sleep = (ms) => {
    return new Promise((resolve) => setTimeout(resolve, ms))
}