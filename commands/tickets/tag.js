const BaseCommand = require('../../utils/structures/BaseCommand')
const Ticket = require('../../src/schemas/TicketSchema');

module.exports = class TicketTagCommand extends BaseCommand {
    constructor () {
        super('tag', 'tickets', [], {
            usage: 'tag',
            description: 'Affiche le tag de l\'utilisateur à l\'origine du ticket',
            categoryDisplayName: `🎫 Tickets`,
            userPermissions: [],
            clientPermissions: [],
            examples: [],
            serverOnly: true,
            admin: false,
            subCommands: false
        });
    }

    async run (client, message, args) {
        const existingDBTicket = await Ticket.findOne({ linkedChannelId: message.channel.id })
        if (existingDBTicket && existingDBTicket.id) {
            let guildMember = message.guild.members.cache.get(existingDBTicket.userId)
            if (!guildMember) return message.channel.send(`**⚠ | **Cet utilisateur a quitté le serveur !`)
            else message.channel.send(`<@!${existingDBTicket.userId}>`)
        } else {
            message.channel.send(`**❌ | **Cette commande peut uniquement être utilisée dans un ticket !`)
        }
    }
}
