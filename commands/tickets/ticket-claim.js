const BaseCommand = require('../../utils/structures/BaseCommand')
const Discord = require('discord.js');
const Ticket = require('../../src/schemas/TicketSchema');

module.exports = class TicketClaimCommand extends BaseCommand {
    constructor() {
        super('ticket-claim', 'tickets', [], {
            usage: 'ticket claim',
            description: `Vous défini comme le modérateur en charge de la résolution de ce ticket`,
            categoryDisplayName: `🎫 Tickets`,
            userPermissions: ['MANAGE_ROLES'],
            clientPermissions: [],
            examples: [],
            serverOnly: true,
            admin: false,
            subCommands: false
        });
    }

    async run(client, message, args) {
        const existingDBTicket = await Ticket.findOne({ linkedChannelId: message.channel.id })
        if (existingDBTicket && existingDBTicket.id) {
            let claimedUser = message.guild.members.cache.get(existingDBTicket.claimedByUserId)
            if (claimedUser) return message.channel.send(`**❌ | **Ce ticket a déja été attribué à \`${claimedUser.user.username}\``)
            Ticket.updateOne({ linkedChannelId: message.channel.id }, { claimedByUserId: message.author.id }, {}, async (err) => {
                if (err) throw err; 
            })
            let chanArgs = message.channel.name.split('┃')
            await message.channel.setName(`🔗┃${chanArgs[1]}`)
            message.channel.overwritePermissions([
                {
                    id: message.guild.roles.everyone.id,
                    deny: ['VIEW_CHANNEL']
                },
                {
                    id: message.author.id,
                    allow: ['VIEW_CHANNEL']
                }
            ])
            message.channel.send(`**✅ | **Ticket assigné à \`${message.author.username}\``)
        } else {
            message.channel.send(`**❌ | **Cette commande peut uniquement être utilisée dans un ticket !`)
        }
    }
}
