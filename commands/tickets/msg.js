const BaseCommand = require('../../utils/structures/BaseCommand')
const Ticket = require('../../src/schemas/TicketSchema');

module.exports = class TicketMsgCommand extends BaseCommand {
    constructor () {
        super('msg', 'tickets', [], {
            usage: 'msg <message>',
            description: 'Répond à un utilisateur dans un ticket',
            categoryDisplayName: `🎫 Tickets`,
            userPermissions: [],
            clientPermissions: [],
            examples: [],
            serverOnly: true,
            admin: false,
            subCommands: false
        });
    }

    async run (client, message, args) {
        const guildMember = message.guild.members.cache.get(message.author.id)
        const existingDBTicket = await Ticket.findOne({ linkedChannelId: message.channel.id })

        if (existingDBTicket && existingDBTicket.id) {
            let guild = message.guild
            args.shift()
            let stringArgs = args.join(' ')
            let dmUser = guild.members.cache.get(existingDBTicket.userId)
            let attachFiles = []
            message.attachments.each(attachment => attachFiles.push(attachment))
            dmUser.createDM().then(dmChannel => {
                try {
                    dmChannel.send(`\`${guildMember.nickname} :\` ${stringArgs}`, {
                        files: attachFiles
                    })
                    message.react('🌐')
                } catch (error) {
                    console.log(error)
                    message.react('❌')
                }
            })
        } else {
            message.channel.send(`**❌ | **Cette commande peut uniquement être utilisée dans un ticket !`)
        }
    }
}
