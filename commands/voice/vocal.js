const BaseCommand = require('../../utils/structures/BaseCommand')
const Discord = require('discord.js');

module.exports = class VocalCommand extends BaseCommand {
    constructor() {
        super('vocal', 'voice', [], {
            usage: 'vocal <commands>',
            description: 'Opération sur les auto channels',
            categoryDisplayName: `🔊 Auto Channels`,
            userPermissions: [],
            clientPermissions: [],
            examples: [],
            serverOnly: true,
            admin: false,
            subCommands: true,
            arguments: `\`commands\` : private, add, remove`,
        });
    }

    async run(client, message, args) {
        message.channel.send(`**❌ | **Arguments invalides ! \`\`${client.config.get(message.guild.id).prefix}help vocal\`\` pour voir les arguments disponibles !`)
    }
}