const BaseCommand = require('../../utils/structures/BaseCommand')
const Discord = require('discord.js');

module.exports = class VocalAddCommand extends BaseCommand {
    constructor() {
        super('vocal-add', 'voice', [], {
            usage: 'vocal add <utilisateur>',
            description: 'Ajoute un utilisateur à votre channel actuel',
            categoryDisplayName: `🔊 Auto Channels`,
            userPermissions: [],
            clientPermissions: [],
            examples: [],
            serverOnly: true,
            admin: false,
            subCommands: false
        });
    }

    async run(client, message, args) {
     if (!args[1]) return message.channel.send(`**:x: | **Vous devez spécifier un utilisateur à ajouter !`)
      let guild = message.guild
      let allmembers = guild.members.cache
      
      const targetMember = await allMembers.find(member => member.user.tag.toLowerCase().includes(args[1].toLowerCase()))
      if (!targetMember) await getMemberFromMentions(message)
      

      let invocationChannel = allmembers.get(message.author.id).voice.channel

      if (!invocationChannel) return message.channel.send(`**:x: | **Vous devez être connecté à un channel vocal pour exécuter cette commande !`)
      if (!invocationChannel.name.includes('┃Vocal de')) return message.channel.send(`**:x: | **Cette commande ne peut pas être utilisée dans ce channel !`)
      if (!invocationChannel.name.toLowerCase().includes(message.author.username.toLowerCase())) return message.channel.send(`**:x: | **Vous n'etes pas le prioriétaire du channel !`)
      try {
          invocationChannel.updateOverwrite(targetMember.user, { CONNECT: true })
          message.channel.send(`**:white_check_mark: | **\`${targetMember.user.username}\` a été ajouté au channel !`)
        } catch (error) { 
          message.channel.send(`**:x: | **Impossible d'ajouter \`${targetMember.user.username}\``)
        }
    }
}

function getMemberFromMentions(message) {
    let targetMembers = message.mentions.members.filter(m => !m.user.bot)

    if (targetMembers.size === 0) {
        return message.channel.send(`**:x: | **Veuillez renseigner un utilisateur valide \`(vocal add <user>)\``)
   } else if (targetMembers.size > 1) {
        return message.channel.send(`**:x: | **Veuillez renseigner un unique utilisateur \`(vocal add <user>)\``)
    } else if (targetMembers.size === 1) {
        return targetMembers.first()
    }
}