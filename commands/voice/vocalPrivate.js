const BaseCommand = require('../../utils/structures/BaseCommand')
const Discord = require('discord.js');

module.exports = class VocalCommand extends BaseCommand {
    constructor() {
        super('vocal-private', 'voice', [], {
            usage: 'vocal private',
            description: 'Privatise le channel dans lequel vous êtes',
            categoryDisplayName: `🔊 Auto Channels`,
            userPermissions: [],
            clientPermissions: [],
            examples: [],
            serverOnly: true,
            admin: false,
            subCommands: false
        });
    }

    async run(client, message, args) {
      
    }
}