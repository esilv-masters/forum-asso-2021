const BaseCommand = require('../../utils/structures/BaseCommand')
const Ticket = require('../../src/schemas/TicketSchema');

module.exports = class TicketMsgCommand extends BaseCommand {
    constructor () {
        super('addrole', 'moderation', [], {
            usage: 'addrole <user> <role>',
            description: 'Ajoute un role à un utilisateur',
            categoryDisplayName: `🎫 Moderation`,
            userPermissions: ['MANAGE_ROLES'],
            clientPermissions: [],
            examples: [],
            serverOnly: true,
            admin: false,
            subCommands: false
        });
    }

    async run (client, message, args) {
        const loading = bot.emojis.cache.get('741276138319380583')
        const allMembers = message.guild.members.cache
        const allRoles = message.guild.roles.cache

        if (args[2]) {
            let targetMember = allMembers.find(m => m.user.tag.toLowerCase().includes(args[1].toLowerCase()))
            if (!targetMember) targetMember = getMemberFromMentions(message)

            args.splice(0, 2)
            let stringRole = args.join(' ')
            let role = allRoles.find(r => r.name.toLowerCase().includes(stringRole.toLowerCase()) && !r.name.includes(`☑️`))

            if (!role) role = getRoleFromMentions(message)

            try {
                targetMember.roles.add(role)
                message.channel.send(`**:white_check_mark: | **Vous avez ajouté le role \`${role.name}\` à \`${selectedmember.user.username}\``)
            } catch (error) {
                console.error(error)
                message.channel.send(`**:x: | **INTERNAL SERVER ERROR`)
            }
        }
    }
}

function getMemberFromMentions(message) {
    let targetMembers = message.mentions.members.filter(m => !m.user.bot)

    if (targetMembers.size === 0) {
        return message.channel.send(`**:x: | **Veuillez renseigner un utilisateur valide \`(adduser <user> <role>)\``)
    } else if (targetMembers.size > 1) {
        return message.channel.send(`**:x: | **Veuillez renseigner un unique utilisateur \`(adduser <user> <role>)\``)
    } else if (targetMembers.size === 1) {
        return targetMembers.first()
    }
}

function getRoleFromMentions(message) {
    let targetRoles = message.mentions.roles

    if (targetRoles.size === 0) {
        return message.channel.send(`**:x: | **Veuillez renseigner un rôle valide \`(adduser <user> <role>)\``)
    } else if (targetRoles.size > 1) {
        return message.channel.send(`**:x: | **Veuillez renseigner un unique rôle \`(adduser <user> <role>)\``)
    } else if (targetRoles.size === 1) {
        return targetRoles.first()
    }
}