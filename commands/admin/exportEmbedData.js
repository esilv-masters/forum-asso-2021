const BaseCommand = require('../../utils/structures/BaseCommand')
const Discord = require('discord.js');
const mongoose = require('mongoose');
const fs = require('fs-extra')

module.exports = class ExportEmbedDataCommand extends BaseCommand {
    constructor() {
        super('exportembed', 'admin', [], {
            usage: 'exportembed',
            description: 'exportembed',
            categoryDisplayName: `🔺 Admin`,
            userPermissions: ['ADMINISTRATOR'],
            clientPermissions: [],
            examples: [],
            serverOnly: true,
            admin: true,
            subCommands: false
        });
    }

    async run(client, message, args) {
        return
        const AllAsso = await mongoose.model('Asso').find({})

        const arrayToExport = []

        for (let i = 0; i < AllAsso.length; i++) {
            let Asso = AllAsso[i];

            await arrayToExport.push({
                title: `${Asso.emoji} ==> ${Asso.name}`,
                color: Asso.color,
                description: Asso.description_fr,
                category: Asso.category,
            })
        }

        let toWrite = {
            embeds: arrayToExport
        }

        await message.channel.send(`Done !`)

        fs.writeFile('embeds.json', JSON.stringify(toWrite))
    }
}

const sleep = async (ms) => {
    return new Promise((resolve) => setTimeout(resolve, ms))
}
