const BaseCommand = require('../../utils/structures/BaseCommand')
const Discord = require('discord.js');
const mongoose = require('mongoose');

module.exports = class NukeMembersCommand extends BaseCommand {
    constructor() {
        super('nukemembers', 'admin', [], {
            usage: 'nukemembers',
            description: 'Nuke tout les roles',
            categoryDisplayName: `🔺 Admin`,
            userPermissions: ['ADMINISTRATOR'],
            clientPermissions: [],
            examples: [],
            serverOnly: true,
            admin: true,
            subCommands: false
        });
    }

    async run(client, message, args) {
        await message.guild.members.fetch()
        const loading = client.emojis.cache.get('741276138319380583')

        const allMembers = message.guild.members.cache
        const allRoles = message.guild.roles.cache

        let msg = await message.channel.send(`**${loading} | **Nuking members...`)

        for (const [id, member] of allMembers.entries()) {
            if (member.bot) continue
            if (member.roles.cache.has('731539610605453405') || member.roles.cache.has('880915963220987994')) continue
    

            let roles = member.roles

            let rolesToRemove = roles.cache.filter(role => role.rawPosition < allRoles.get('880915963220987994').rawPosition && role.rawPosition > allRoles.get('753030184751333376').rawPosition || role.rawPosition < allRoles.get('736982911735300138').rawPosition && role.rawPosition > allRoles.get('806846373827575828').rawPosition || role.id === '731532207772139521')

            const User = await mongoose.model('User').findOne({ discordId: member.user.id })

            if (User && User.id) {
                User.assosAsStaff = []
                User.assosAsVisitor = []

                await User.save();
                console.log(`${member.user.username} => Asso config nuked!`)
            }

            if (rolesToRemove.size > 0) {
                await member.roles.remove(rolesToRemove)
                console.log(`${member.user.username} => ${rolesToRemove.size} roles removed !`)
            }
            
        }

        await msg.edit(`**✅ | **Members nuked !`)
    }
}

const sleep = async (ms) => {
    return new Promise((resolve) => setTimeout(resolve, ms))
}
