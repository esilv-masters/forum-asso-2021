const BaseCommand = require('../../utils/structures/BaseCommand')
const Discord = require('discord.js');
const mongoose = require('mongoose');

module.exports = class AddModCOmmand extends BaseCommand {
    constructor() {
        super('mod', 'admin', [], {
            usage: 'mod <utilisateur>',
            description: 'Ajoute un utilisateur en tant que modérateur',
            categoryDisplayName: `🔺 Admin`,
            userPermissions: ['ADMINISTRATOR'],
            clientPermissions: [],
            examples: [],
            serverOnly: true,
            admin: true,
            subCommands: false
        });
    }

    async run(client, message, args) {
        message.guild.members.fetch()
        const allMembers = message.guild.members.cache

        let guildMember = allMembers.find(m => m.user.tag.toLowerCase().includes(args[1].toLowerCase()));
        if (!guildMember) {
            let targetMembers = message.mentions.members.filter(m => !m.user.bot)

            if (targetMembers.size === 0) {
                return message.channel.send(`**❌ | **Veuillez selectionner au moins un utilisateur !`)
            } else if (targetMembers.size > 1) {
                return message.channel.send(`**❌ | **Veuillez selectionner qu'un seul utilisateur à ajouter en tant que modérateur`)
            } else if (targetMembers.size === 1) {
                guildMember = targetMembers.first()
            }
        }

        if (guildMember) {
            const User = await mongoose.model('User').findOne({ discordId: guildMember.user.id });

            if (User && User.id) {
                User.isMod = true;
                User.save();
                await guildMember.roles.add('731539610605453405')
                message.channel.send(`**✅ | **\`\`${guildMember.user.username}\`\` a bien été ajouté en tant que modérateur !`)
            } else {
                message.channel.send(`**❌ | **INTERNAL SERVER ERROR : DB CORRUPTION`)
            }
        } else {
            message.channel.send(`**❌ | **Utilisateur introuvable !`)
        }
    }
}

