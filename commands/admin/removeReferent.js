const BaseCommand = require('../../utils/structures/BaseCommand')
const Discord = require('discord.js');
const mongoose = require('mongoose');
const { unsubscribe } = require('../../backend/routes');

module.exports = class RemoveReferentCommand extends BaseCommand {
    constructor() {
        super('delreferent', 'admin', ['dereferent', 'removereferent'], {
            usage: 'delreferent <utilisateur>',
            description: 'Supprime un utilisateur de la liste des référents',
            categoryDisplayName: `🔺 Admin`,
            userPermissions: ['BAN_MEMBERS'],
            clientPermissions: [],
            examples: [],
            serverOnly: true,
            admin: false,
            subCommands: false
        });
    }

    async run(client, message, args) {
        const allMembers = message.guild.members.cache

        let guildMember = allMembers.find(m => m.user.tag.toLowerCase().includes(args[1].toLowerCase()));
        if (!guildMember) {
            let targetMembers = message.mentions.members.filter(m => !m.user.bot)

            if (targetMembers.size === 0) {
                return message.channel.send(`**❌ | **Veuillez selectionner au moins un utilisateur !`)
            } else if (targetMembers.size > 1) {
                return message.channel.send(`**❌ | **Veuillez selectionner qu'un seul utilisateur à ajouter en tant que modérateur`)
            } else if (targetMembers.size === 1) {
                guildMember = targetMembers.first()
            }
        }

        if (guildMember) {
            const User = await mongoose.model('User').findOne({ discordId: guildMember.user.id });

            if (User && User.id) {

                if (User.isReferent === false) return message.channel.send(`**❌ | **L'utilisateur selectionné n'est pas référent !`)

                const Asso = await mongoose.model('Asso').findOne({ referentId:guildMember.user.id });
                if (Asso && Asso.id ) {
                    Asso.referentId = undefined
                    Asso.referentName = undefined
                    User.isReferent = false;

                    User.save();
                    Asso.save();
                    await guildMember.roles.remove('880915963220987994')
                    message.channel.send(`**✅ | **\`\`${guildMember.user.tag}\`\` n'est plus référent de \`\`${Asso.name}\`\``)
                } else {
                    message.channel.send(`**❌ | **INTERNAL SERVER ERROR : DB CONFLICT`)
                }
            } else {
                message.channel.send(`**❌ | **INTERNAL SERVER ERROR : DB CORRUPTION`)
            }
        } else {
            message.channel.send(`**❌ | **Utilisateur introuvable !`)
        }
    }
}

