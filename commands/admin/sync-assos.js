const BaseCommand = require('../../utils/structures/BaseCommand');
const Asso = require('../../src/schemas/AssoSchema');

const {
    getEmoji,
    removeEmojis
} = require('../../utils/functions/utilitaryFunctions');

module.exports = class SyncAssosCommand extends BaseCommand {
    constructor() {
        super('sync-assos', 'admin', [], {
            usage: 'sync assos',
            description: 'Synchronise toutes les assos du serveur avec la DB',
            categoryDisplayName: `🔺 Admin`,
            userPermissions: ['ADMINISTRATOR'],
            clientPermissions: [],
            examples: [],
            serverOnly: true,
            admin: true,
            subCommands: false
        });
    }

    async run(client, message, args) {
        await message.guild.roles.fetch()

        const allRoles = message.guild.roles.cache

        const dividerLowAssoRoles = allRoles.get('806846373827575828').rawPosition
        const dividerHighAssoRoles = allRoles.get('806192779230707772').rawPosition
        
        const assoRoles = allRoles.filter(r => r.rawPosition > dividerLowAssoRoles && r.rawPosition < dividerHighAssoRoles && !r.name.includes('▬▬'));

        

        const arrayOfAssos = assoRoles.array();

        for (let i = 0; i < arrayOfAssos.length; i++) {
            let assoRole = arrayOfAssos[i];

            const existingDBAsso = await Asso.findOne({ linkedRoleId: assoRole.id });

            if (!existingDBAsso) {
                let assoName = removeEmojis(assoRole.name).trim();
                let assoEmoji = getEmoji(assoRole.name)
    
                try {
                    Asso.create({
                        linkedRoleId: assoRole.id,
                        name: assoName,
                        emoji: assoEmoji,
                        color: assoRole.hexColor,
                        category: 'Inconnue'
                    })
                } catch (err) {
                    console.error(err);
                }
            } else {
                let assoName = removeEmojis(assoRole.name).trim();
                let assoEmoji = getEmoji(assoRole.name)

                Asso.updateOne({ linkedRoleId: assoRole.id }, {
                    name: assoName,
                    emoji: assoEmoji,
                    color: assoRole.hexColor
                })
            }
            
            
        }
    }
}