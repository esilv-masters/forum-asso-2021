const BaseCommand = require('../../utils/structures/BaseCommand')
const Discord = require('discord.js');
const mongoose = require('mongoose');
const fs = require('fs-extra');
const JSONData = require('../../assos.json')

module.exports = class CompareAssosJsonCommand extends BaseCommand {
    constructor() {
        super('compareassos', 'admin', [], {
            usage: 'compareassos',
            description: '',
            categoryDisplayName: `🔺 Admin`,
            userPermissions: ['ADMINISTRATOR'],
            clientPermissions: [],
            examples: [],
            serverOnly: true,
            admin: true,
            subCommands: false
        });
    }

    async run(client, message, args) {
        const allMembers = message.guild.members.cache
        const allRoles = message.guild.roles.cache
        for (let i = 0; i < JSONData.length; i++) {
            let asso = JSONData[i];

            let referent = allMembers.find(member => member.user.tag === asso.referent_disc_id)

            let assoRole = allRoles.find(role => role.name.toLowerCase().includes(asso.fullname.toLowerCase()))


            if (assoRole) {
                const Asso = await mongoose.model('Asso').findOne({ linkedRoleId: assoRole.id })

                if (Asso && Asso.id) {
                    Asso.description_fr = asso.description_fr
                    Asso.description_en = asso.description_en
        
                    Asso.save()
                } else {
                    console.log(asso.fullname)
                }
                
                if (!referent) console.log(`${asso.fullname}✔✔`)
                else if (referent) console.log(`${asso.fullname}✔✔✔`)
            } else if (!assoRole) {
                if (referent) {
                    console.log(`${asso.fullname}✔`)
                } else {
                    console.log(`${asso.fullname}`)
                }
            }


        }
    }
}

