const BaseCommand = require('../../utils/structures/BaseCommand')
const Discord = require('discord.js');
const {
    removeEmojis
} = require('../../utils/functions/utilitaryFunctions');

const User = require('../../src/schemas/UserSchema');
const UserArchive = require('../../src/schemas/UserArchiveSchema');
const mongoose =  require('mongoose');

module.exports = class SyncUsersCommand extends BaseCommand {
    constructor() {
        super('sync-users', 'admin', [], {
            usage: 'sync users',
            description: 'Synchronise les utilisateurs sur le serveur avec la DB',
            categoryDisplayName: `🔺 Admin`,
            userPermissions: ['ADMINISTRATOR'],
            clientPermissions: [],
            examples: [],
            serverOnly: true,
            admin: true,
            subCommands: false
        });
    }

    async run(client, message, args) {
        const loading = client.emojis.cache.get('741276138319380583')

        await message.guild.members.fetch();
        await message.guild.roles.fetch();

        const allMembers = message.guild.members.cache
        const allRoles = message.guild.roles.cache

        const arrayOfMembers = allMembers.filter(m => !m.user.bot).array();

        const messages = []

        const dividierLowSchoolRoles = allRoles.get('806199907425845309').rawPosition
        const dividerHighSchoolRoles = allRoles.get('753030181530370168').rawPosition

        const dividerLowAssoRoles = allRoles.get('806846373827575828').rawPosition
        const dividerHighAssoRoles = allRoles.get('806192779230707772').rawPosition

        const schoolRoles = allRoles.filter(r => r.rawPosition > dividierLowSchoolRoles && r.rawPosition < dividerHighSchoolRoles)
        
        const assoRoles = allRoles.filter(r => r.rawPosition > dividerLowAssoRoles && r.rawPosition < dividerHighAssoRoles && !r.name.includes('▬▬'));

        const Users = await mongoose.model('User').find({});

        let msg = await message.channel.send(`**${loading} | **Checking Database ...`)

        for (let i = 0; i < arrayOfMembers.length; i++) {
            let member = arrayOfMembers[i];
            let memberRoles = member.roles.cache
            let memberObject = {
                assos: [],
                isMod: false,
                isSpMod: false,
                isAdmin: false,
                school: undefined,
                year: undefined
            }

            memberObject.isMod = memberRoles.has('731539610605453405') ? true : false;
            memberObject.isSpMod = memberRoles.has('753699877766037504') ? true : false;
            memberObject.isAdmin = memberRoles.has('751494550958375075') ? true : false;


            let memberSchoolRoles = memberRoles.intersect(schoolRoles);
            let memberAssoRoles = memberRoles.intersect(assoRoles);

            if (memberSchoolRoles && memberSchoolRoles.size === 1) {
                let schoolRole = memberSchoolRoles.first()
                
                memberObject.year = schoolRole.name.match(/\d/g)[0]

                for (const [id, role] of memberAssoRoles) {
                    let assoObject = {
                        assoName: removeEmojis(role.name).trim(),
                        userIsBan: false,
                        userStatus: 'pending'
                    }
                    memberObject.assos.push(assoObject)
                }
                // 731532207772139521 = Role étudiant
            } else if (memberSchoolRoles && memberSchoolRoles.size > 1 && memberRoles.has('731532207772139521')) {
                console.log(`Multiple school roles for ${member.user.username}`)
                let schoolRole = memberSchoolRoles.first()
                
                memberObject.year = schoolRole.name.match(/\d/g)[0]

                for (const [id, role] of memberAssoRoles) {
                    let assoObject = {
                        assoName: removeEmojis(role.name).trim(),
                        userIsBan: false,
                        userStatus: 'pending'
                    }
                    memberObject.assos.push(assoObject)
                }
            } else if (memberRoles.has('731532207772139521')) {
                console.log(`No school roles for ${member.user.username}`)
                for (const [id, role] of memberAssoRoles) {
                    let assoObject = {
                        assoName: removeEmojis(role.name).trim(),
                        userIsBan: false,
                        userStatus: 'pending'
                    }
                    memberObject.assos.push(assoObject)
                }
            } else {
                console.log('Not a student')
            }

            const existingDBUser = await mongoose.model('User').findOne({ discordId: member.user.id })

            if (existingDBUser && existingDBUser.id) {
            } else {
                try {
                    await User.create({
                        username: member.user.username,
                        discordId: member.user.id,
                        avatarURL: member.user.displayAvatarURL(),
                        isMod: memberObject.isMod,
                        isSpMod: memberObject.isSpMod,
                        isAdmin: memberObject.isAdmin,
                        school: memberObject.school,
                        schoolYear: memberObject.year,
                        assosAsStaff: memberObject.assos
                    })
                } catch (err) {
                    console.error(err)
                }
                messages.push(`**✅ | **Nouvelle entrée dans la DB :  \`${member.user.username}\``)
            }
        }

        for (const user of Users) {
            let linkedGuildMember = await allMembers.get(user.discordId)

            if (!linkedGuildMember) {
                await mongoose.model('User').deleteOne({ discordId: user.discordId });
                const ArchivedUser = await UserArchive.findOne({ discordId: user.discordId });
                if (ArchivedUser && ArchivedUser.id) {
                    await UserArchive.deleteOne({ discordId: user.discordId })
                }
                await UserArchive.insertMany(user)
                messages.push(`**❌ | **L'utilisateur \`${user.username}\` a quitté le serveur, entrée effacée dans la DB !`)
            }
        }

        var sortstring = function (a, b)    {
            a = a.toLowerCase();
            b = b.toLowerCase();
            if (a.startsWith('**✅') && b.startsWith('**:warning:')) return -1
            if (a.startsWith('**✅') && b.startsWith('**❌')) return -1
            if (a.startsWith('**:warning:') && b.startsWith('**❌')) return -1
            if (b.startsWith('**✅') && a.startsWith('**:warning:')) return 1
            if (b.startsWith('**✅') && a.startsWith('**❌')) return 1
            if (b.startsWith('**:warning:') && a.startsWith('**❌')) return 1
            return 0;
        }

        messages.sort(sortstring)

        if (messages.length > 0) {
            await msg.edit('**⚠ | **Erreurs trouvées :')
            await message.channel.send(messages.join('\n'), {
                split: true
            })
        } else {
            await msg.edit(`**✅ | **Configuration correcte !`)
        }
        
    }
}