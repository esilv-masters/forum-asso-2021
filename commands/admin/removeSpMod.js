const BaseCommand = require('../../utils/structures/BaseCommand')
const Discord = require('discord.js');
const mongoose = require('mongoose');

module.exports = class RemoveModCommand extends BaseCommand {
    constructor() {
        super('desupermod', 'admin', [], {
            usage: 'desupermod <utilisateur>',
            description: 'Retire un utilisateur de la liste des super modérateurs',
            categoryDisplayName: `🔺 Admin`,
            userPermissions: ['ADMINISTRATOR'],
            clientPermissions: [],
            examples: [],
            serverOnly: true,
            admin: true,
            subCommands: false
        });
    }

    async run(client, message, args) {
        const allMembers = message.guild.members.cache
        
        let guildMember = allMembers.find(m => m.user.tag.toLowerCase().includes(args[1].toLowerCase()));
        if (!guildMember) {
            let targetMembers = message.mentions.members.filter(m => !m.user.bot)

            if (targetMembers.size === 0) {
                return message.channel.send(`**❌ | **Veuillez selectionner au moins un utilisateur !`)
            } else if (targetMembers.size > 1) {
                return message.channel.send(`**❌ | **Veuillez selectionner qu'un seul utilisateur à retirer de la liste des super modérateurs !`)
            } else if (targetMembers.size === 1) {
                guildMember = targetMembers.first()
            }
        }

        if (guildMember) {
            const User = await mongoose.model('User').findOne({ discordId: guildMember.user.id })
            if (User && User.isSpMod === false) {
                return message.channel.send(`**⚠ | **L'utilisateur selectionné n'est pas sur la liste des super modérateurs !`)
            } else if (User && User.isSpMod === true) {
                User.isMod = false
                User.isSpMod = false
                User.save()
                await guildMember.roles.remove('731539610605453405')
                await guildMember.roles.remove('753699877766037504')
                message.channel.send(`**✅ | **\`\`${guildMember.user.tag}\`\` a bien été retiré de la liste des super modérateurs !`)
            } else {
                message.channel.send(`**❌ | **INTERNAL SERVER ERROR : DB CORRUPTION`)
            }
        } else {
            message.channel.send(`**❌ | **Utilisateur introuvable !`)
        }
    }
}

