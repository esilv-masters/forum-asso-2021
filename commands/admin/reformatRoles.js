const BaseCommand = require('../../utils/structures/BaseCommand')
const Discord = require('discord.js');
const mongoose = require('mongoose');

module.exports = class ReformatRolesCommand extends BaseCommand {
    constructor() {
        super('reformatroles', 'admin', [], {
            usage: 'reformatroles',
            description: 'reformatroles',
            categoryDisplayName: `🔺 Admin`,
            userPermissions: ['ADMINISTRATOR'],
            clientPermissions: [],
            examples: [],
            serverOnly: true,
            admin: true,
            subCommands: false
        });
    }

    async run(client, message, args) {
        return
        await message.guild.roles.fetch()

        const allRoles = message.guild.roles.cache

        for (const [key, value] of allRoles.entries()) {
            if (value.rawPosition < allRoles.get('736982718574887003').rawPosition) {
                let permissions = value.permissions
                let newPermissions = await permissions.remove(['SEND_TTS_MESSAGES', 'CHANGE_NICKNAME'])

                await value.edit({
                    permissions: newPermissions
                })

                console.log(`${value.name} has been updated`)
            }
        }
    }
}

const sleep = async (ms) => {
    return new Promise((resolve) => setTimeout(resolve, ms))
}
