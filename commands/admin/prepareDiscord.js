const BaseCommand = require('../../utils/structures/BaseCommand')
const Discord = require('discord.js');
const mongoose = require('mongoose');

module.exports = class PrepareDiscordCommand extends BaseCommand {
    constructor() {
        super('initiate', 'admin', [], {
            usage: 'initiate',
            description: 'initiate',
            categoryDisplayName: `🔺 Admin`,
            userPermissions: ['ADMINISTRATOR'],
            clientPermissions: [],
            examples: [],
            serverOnly: true,
            admin: true,
            subCommands: false
        });
    }

    async run(client, message, args) {
        await message.guild.members.fetch()

        const allMembers = message.guild.members.cache
        const allRoles = message.guild.roles.cache
        const allChannels = message.guild.channels.cache

        const toNukeChannels = allChannels.filter(channel => channel.type != 'category' && (channel.parent.id === '741325711582953592' || channel.parent.id === '806256513068826684' || channel.parent.id === '807295185901060127' || channel.parent.id === '806256544484294656'))

        const toPrepareChannels = allChannels.filter(channel => channel.type != 'category' && (channel.parent.id === '806178765034094652' || channel.parent.id === '806178816606339092' || channel.parent.id === '806178868172029952'))


        for (const [id, channel] of toPrepareChannels.entries()) {
            let permissionOverwrites = await channel.permissionOverwrites.toJSON()

            permissionOverwrites.push(
                {
                    id: '731539610605453405',
                    allow: ['VIEW_CHANNEL', 'SEND_MESSAGES', 'CONNECT']
                },
                {
                    id: '753699877766037504',
                    allow: ['VIEW_CHANNEL', 'SEND_MESSAGES', 'CONNECT']
                }
            )

            await channel.edit({ permissionOverwrites: permissionOverwrites })
        }
        // console.log(toNukeChannels)
        // for (const [id, channel] of toNukeChannels.entries()) {
        //     await channel.overwritePermissions([
        //         {
        //             id: message.guild.roles.everyone.id,
        //             deny: ['VIEW_CHANNEL']
        //         },
        //         {
        //             id: '753761060825333832',
        //             deny: ['SEND_MESSAGES']
        //         },
        //         {
        //             id: '731532207772139521',
        //             allow: ['VIEW_CHANNEL']
        //         },
        //         {
        //             id: '806200261357994016',
        //             allow: ['VIEW_CHANNEL']
        //         },
        //         {
        //             id: '731539610605453405',
        //             allow: ['VIEW_CHANNEL', 'SEND_MESSAGES', 'CONNECT']
        //         },
        //         {
        //             id: '753699877766037504',
        //             allow: ['VIEW_CHANNEL', 'SEND_MESSAGES', 'CONNECT']
        //         }
        //     ])
        // }

        await message.channel.send(`Nuked`)
    }
}

const sleep = async (ms) => {
    return new Promise((resolve) => setTimeout(resolve, ms))
}
