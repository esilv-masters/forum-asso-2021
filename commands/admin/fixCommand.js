const BaseCommand = require('../../utils/structures/BaseCommand')
const Discord = require('discord.js');
const mongoose = require('mongoose');

module.exports = class createVisitorRolesCommand extends BaseCommand {
    constructor() {
        super('createvisitorroles', 'admin', [], {
            usage: 'createvisitorroles',
            description: 'createvisitorroles',
            categoryDisplayName: `🔺 Admin`,
            userPermissions: ['ADMINISTRATOR'],
            clientPermissions: [],
            examples: [],
            serverOnly: true,
            admin: true,
            subCommands: false
        });
    }

    async run(client, message, args) {
        const AllAsso = await mongoose.model('Asso').find({})

        const allChannels = message.guild.channels.cache
        const allRoles = message.guild.roles.cache

        for (let i = 0; i < AllAsso.length; i++) {
            let Asso = AllAsso[i];

            let existingVisitorRole = allRoles.find(role => role.name === `☑️ ${Asso.name}`)
            if (!existingVisitorRole) {
                await message.guild.roles.create().then(createdRole => {
                    createdRole.edit({
                        name: `☑️️ ${Asso.name}`,
                        color: '#3075fa'
                    })
                })
                console.log(`Created visitor role for ${Asso.name}`)
            }
            let existingCategory = allChannels.find(channel => channel.name.toLowerCase().includes(Asso.name.toLowerCase()) && channel.type === 'category');
            if (!existingCategory) {
                await message.guild.channels.create(`${Asso.emoji} | ${Asso.name}`, {
                    type: 'category',
                    permissionOverwrites: [
                        {
                            id: message.guild.roles.everyone.id,
                            deny: ['VIEW_CHANNEL']
                        }
                    ]
                }).then(async (createdChannel) => {
                    await mongoose.model('Asso').updateOne({ name: Asso.name }, { linkedCategoryId: createdChannel.id })
                })
                console.log(`Created category for ${Asso.name}`)
            }
        }

        await message.channel.send(`Done !`)
    }
}

const sleep = async (ms) => {
    return new Promise((resolve) => setTimeout(resolve, ms))
}
