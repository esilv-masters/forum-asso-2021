const BaseCommand = require('../../utils/structures/BaseCommand')
const Discord = require('discord.js');
const mongoose = require('mongoose');

module.exports = class AddReferentCommand extends BaseCommand {
    constructor() {
        super('referent', 'admin', [], {
            usage: 'referent <utilisateur>',
            description: 'Ajoute un utilisateur en tant que référent',
            categoryDisplayName: `🔺 Admin`,
            userPermissions: ['BAN_MEMBERS'],
            clientPermissions: [],
            examples: [],
            serverOnly: true,
            admin: false,
            subCommands: false
        });
    }

    async run(client, message, args) {
        await message.guild.members.fetch()
        const allMembers = message.guild.members.cache
        const allRoles = message.guild.roles.cache

        let guildMember = allMembers.find(m => m.user.tag.toLowerCase().includes(args[1].toLowerCase()));
        if (!guildMember) {
            let targetMembers = message.mentions.members.filter(mem => !mem.user.bot)

            if (targetMembers.size === 0) {
                return message.channel.send(`**❌ | **Veuillez selectionner au moins un utilisateur !`)
            } else if (targetMembers.size > 1) {
                return message.channel.send(`**❌ | **Veuillez selectionner qu'un seul utilisateur à ajouter en tant que modérateur`)
            } else if (targetMembers.size === 1) {
                guildMember = targetMembers.first()
            }
        }

        if (guildMember) {
            const User = await mongoose.model('User').findOne({ discordId: guildMember.user.id });

            if (User && User.id) {
                const filter = m => m.author.id === message.author.id
                message.channel.send(`Veuillez spécifier le nom de l'asso dont ${User.username} est référent :`)
                let assoString = (await message.channel.awaitMessages(filter, { max: 1, time: 60000, errors: ['time'] }).catch(err => message.channel.send(`**❌ | **Timed Out !`))).first().content

                let assoRole = await message.guild.roles.cache.find(role => role.name.toLowerCase().includes(assoString.toLowerCase()) && !role.name.includes('☑'))
                if (!assoRole) return message.channel.send(`**❌ | **L'asso spécifiée est introuvable !`)

                const Asso = await mongoose.model('Asso').findOne({ linkedRoleId: assoRole.id }).catch(err => console.error(err))
                if (Asso && Asso.id) {
                    if (Asso.referentId === guildMember.user.id) {
                        message.channel.send(`**⚠ | **\`\`${guildMember.user.tag}\`\` est déja référent de \`\`${Asso.name}\`\``)
                    } else if (Asso.referentId) {
                        message.channel.send(`**⚠ | **Il y a déja un référent défini pour \`\`${Asso.name}\`\` : \`\`${Asso.referentName}\`\``)
                    } else {
                        let assoRole = allRoles.get(Asso.linkedRoleId)
                        User.isReferent = true;
                        Asso.referentId = guildMember.user.id;
                        Asso.referentName = guildMember.user.tag;
                        let assoInUserIndex = User.assosAsStaff ? await User.assosAsStaff.findIndex(asso => asso.assoName === Asso.name) : -1
                        if (assoInUserIndex >= 0) User.assosAsStaff[assoInUserIndex].status = 'accepted'
                        else User.assosAsStaff.push({
                            assoName: Asso.name,
                            status: 'accepted',
                            userIsBan: false
                        })
                        Asso.save();
                        User.save();
                        await guildMember.roles.add(['880915963220987994', assoRole.id, '881553681353420821'])
                        message.channel.send(`**✅ | **\`\`${guildMember.user.tag}\`\` a bien été ajouté en tant que référent de \`\`${Asso.name}\`\` !`)
                    }
                } else {
                    message.channel.send(`**❌ | **INTERNAL SERVER ERROR : DB CORRUPTION`)
                }
            } else {
                message.channel.send(`**❌ | **INTERNAL SERVER ERROR : DB CORRUPTION`)
            }
        } else {
            message.channel.send(`**❌ | **Utilisateur introuvable !`)
        }
    }
}

