const BaseCommand = require('../../utils/structures/BaseCommand')
const Discord = require('discord.js');
const mongoose = require('mongoose');
const fs = require('fs-extra')

module.exports = class ReactToMessageCommand extends BaseCommand {
    constructor() {
        super('reactmsg', 'admin', [], {
            usage: 'reactmsg',
            description: 'reactmsg',
            categoryDisplayName: `🔺 Admin`,
            userPermissions: ['ADMINISTRATOR'],
            clientPermissions: [],
            examples: [],
            serverOnly: true,
            admin: true,
            subCommands: false
        });
    }

    async run(client, message, args) {
        let msgToReact = await message.channel.messages.fetch(args[1])
        await args.splice(0, 2)
        let arrayOfReactions = await JSON.parse(args.join(' '))

        if(msgToReact && arrayOfReactions) {
            for(const reaction of arrayOfReactions) {
                await msgToReact.react(reaction)
            }
        }
    }
}