const BaseCommand = require('../../utils/structures/BaseCommand')
const Discord = require('discord.js');

module.exports = class SyncCommand extends BaseCommand {
    constructor() {
        super('sync', 'admin', [], {
            usage: 'sync <commands>',
            description: 'Synchronise des données du serveur avec la DB',
            categoryDisplayName: `🔺 Admin`,
            userPermissions: ['ADMINISTRATOR'],
            clientPermissions: [],
            examples: [],
            serverOnly: true,
            admin: true,
            subCommands: true,
            arguments: `\`commands\` : users, assos`,
        });
    }

    async run(client, message, args) {
        message.channel.send(`**❌ | **Arguments invalides ! \`\`${client.config.get(message.guild.id).prefix}help sync\`\` pour voir les arguments disponibles !`)
    }
}