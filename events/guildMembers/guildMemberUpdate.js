const BaseEvent = require('../../utils/structures/BaseEvent')
const mongoose = require('mongoose')

module.exports = class guildMemberUpdate extends BaseEvent {
    constructor() {
        super('guildMemberUpdate')
    }

    async run(client, oldGuildMember, newGuildMember) {
        if (oldGuildMember.guild.id != '731531911465271417') return;
        if (oldGuildMember.user.bot) return;
        if (oldGuildMember.user.username === newGuildMember.user.username) return;

        const User = await mongoose.model('User').findOne({ discordId: newGuildMember.user.id })

        if (User && User.id) {
            User.username = newGuildMember.user.username
            try {
                await User.save();
            } catch (error) {
                throw error;
            }
        }
    }
}