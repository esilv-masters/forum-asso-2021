const BaseEvent = require('../../utils/structures/BaseEvent')
const mongoose = require('mongoose')
const Discord = require('discord.js')
const UserArchive = require('../../src/schemas/UserArchiveSchema')

module.exports = class guildMemberRemove extends BaseEvent {
    constructor() {
        super('guildMemberRemove')
    }

    async run(client, guildMember) {
        if (guildMember.guild.id != '731531911465271417') return
        const existingDBTicket = await mongoose.model('Ticket').findOne({ userId: guildMember.user.id })
        const User = await mongoose.model('User').findOne({ discordId: guildMember.user.id })

        if (User && User.id) {
            await mongoose.model('User').deleteOne({ discordId: guildMember.user.id });
            const ArchivedUser = await UserArchive.findOne({ discordId: guildMember.user.id });
            if (ArchivedUser && ArchivedUser.id) {
                await UserArchive.deleteOne({ discordId: guildMember.user.id })
            }
            await UserArchive.insertMany(User)
            client.allUsers.delete(User.discordId)
        }

        if (existingDBTicket && existingDBTicket.id) {
            mongoose.model('TicketArchive').create({
                dmChannelId: existingDBTicket.dmChannelId,
                name: existingDBTicket.name,
                userId: existingDBTicket.userId,
                guildId: existingDBTicket.guildId,
                claimedByUserId: existingDBTicket.claimedByUserId || null,
                messages: existingDBTicket.messages
            }, async (err) => {
                if (err) throw err;
                else {
                    let linkedChannel = guildMember.guild.channels.cache.get(existingDBTicket.linkedChannelId);
                    let claimedUser = guildMember.guild.members.cache.get(existingDBTicket.claimedByUserId)
                    if (claimedUser) claimedUser.createDM().then(dmChannel => {
                        dmChannel.send(`**⚠ | **Votre ticket avec \`\`${guildMember.user.tag}\`\` été fermé car il a quitté le serveur : \`\`${guildMember.guild.name}\`\` !`)
                    }).catch(err => console.error(err))
                    await mongoose.model('Ticket').deleteOne({ userId: guildMember.user.id })
                    let deleteEmbed = new Discord.MessageEmbed()
                        .setDescription("Suppression du ticket dans 5 secondes...\nRaison: \`\`L'utilisateur a quitté le serveur !\`\`")
                        .setColor('ff5733')
                    linkedChannel.send(deleteEmbed);
                    await sleep(5000);
                    linkedChannel.delete();
                }
            })
        }
    }
}

const sleep = (ms) => {
    return new Promise((resolve) => {setTimeout(resolve, ms)})
}