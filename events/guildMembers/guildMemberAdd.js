const BaseEvent = require('../../utils/structures/BaseEvent')
const mongoose = require('mongoose')
const Discord = require('discord.js')
const UserArchive = require('../../src/schemas/UserArchiveSchema')

module.exports = class guildMemberAdd extends BaseEvent {
    constructor() {
        super('guildMemberAdd')
    }

    async run(client, guildMember) {
        if (guildMember.guild.id != '731531911465271417') return
        const User = await mongoose.model('User').findOne({ discordId: guildMember.user.id });
        const ArchivedUser = await UserArchive.findOne({ discordId: guildMember.user.id });
        const PreRegistredUser = await mongoose.model('PreRegistredUser').findOne({ discordId: guildMember.user.id });

        const allRoles = guildMember.guild.roles.cache


        if (!(User && User.id)) {
            if (ArchivedUser && ArchivedUser.id) {
                ArchivedUser.username = guildMember.user.username,
                ArchivedUser.avatarURL = guildMember.user.displayAvatarURL();
                if (PreRegistredUser && PreRegistredUser.id) {
                    ArchivedUser.assosAsStaff = PreRegistredUser.assosAsStaff
                    ArchivedUser.assosAsVisitor = PreRegistredUser.assosAsVisitor
                    ArchivedUser.firstName = PreRegistredUser.firstName
                    ArchivedUser.lastName = PreRegistredUser.lastName
                    ArchivedUser.school = PreRegistredUser.school
                    ArchivedUser.schoolYear = PreRegistredUser.schoolYear
                    await mongoose.model('PreRegistredUser').deleteOne({ discordId: guildMember.user.id })
                }

                await ArchivedUser.save();

                if (ArchivedUser.isMod) {
                    await guildMember.roles.add('731539610605453405')
                }
                if (ArchivedUser.isSpMod) {
                    await guildMember.roles.add('753699877766037504')
                }
                if (ArchivedUser.isAdmin) {
                    await guildMember.roles.add('751494550958375075')
                }
                if (ArchivedUser.isReferent) {
                    await guildMember.roles.add('880915963220987994')
                }
                
                await initializeUser(allRoles, guildMember, ArchivedUser);
                await mongoose.model('User').insertMany(ArchivedUser);
                client.allUsers.set(ArchivedUser.discordId, ArchivedUser)
                await UserArchive.deleteOne({ discordId: guildMember.user.id })
            } else {
                const NewUser = await mongoose.model('User').create({
                    username: guildMember.user.username,
                    discordId: guildMember.user.id,
                    avatarURL: guildMember.user.displayAvatarURL(),
                })

                if (PreRegistredUser && PreRegistredUser.id) {
                    NewUser.assosAsStaff = PreRegistredUser.assosAsStaff
                    NewUser.assosAsVisitor = PreRegistredUser.assosAsVisitor
                    NewUser.firstName = PreRegistredUser.firstName
                    NewUser.lastName = PreRegistredUser.lastName
                    NewUser.school = PreRegistredUser.school
                    NewUser.schoolYear = PreRegistredUser.schoolYear
                    await mongoose.model('PreRegistredUser').deleteOne({ discordId: guildMember.user.id })
                }

                await NewUser.save()

                client.allUsers.set(NewUser.discordId, NewUser)

                await initializeUser(allRoles, guildMember, NewUser);
            }
        }
    }
}

async function initializeUser(allRoles, guildMember, ArchivedUser) {
    if (ArchivedUser.school && ArchivedUser.schoolYear && ArchivedUser.firstName && ArchivedUser.lastName) {
        let rolesToAdd = []
        let schoolRole = await allRoles.find(r => r.name.includes(`${ArchivedUser.school} - A${ArchivedUser.schoolYear}`))
        if (schoolRole) rolesToAdd.push(schoolRole.id, '731532207772139521') //PUSH SCHOOL ROLE && ETUDIANT
        if (ArchivedUser.assosAsStaff && ArchivedUser.assosAsStaff.length > 0) { // A REVOIR
            for (let i = 0; i < ArchivedUser.assosAsStaff.length; i++) {
                let assoAsStaff = ArchivedUser.assosAsStaff[i];
                if (assoAsStaff.status === 'accepted') {
                    let staffRole = await getStaffRoleByName(`${assoAsVisitor.assoName}`)
                    const Asso = await mongoose.model('Asso').findOne({ linkedRoleId: staffRole.id })
                    if (Asso) rolesToAdd.push(Asso.linkedRoleId, '881553681353420821') //PUSH ASSO AS STAFF ROLE
                }
            }
        }
        if (ArchivedUser.assosAsVisitor && ArchivedUser.assosAsVisitor.length > 0) {
            for (let i = 0; i < ArchivedUser.assosAsVisitor.length; i++) {
                let assoAsVisitor = ArchivedUser.assosAsVisitor[i];
                let visitorRole = await getVisitorRoleByName(`${assoAsVisitor.assoName}`)
                if (visitorRole) rolesToAdd.push(visitorRole.id) //PUSH ASSO AS VISITOR ROLE
            }
        }

        let rolesToAddClean = rolesToAdd.filter(function(elem, pos) {
            return rolesToAdd.indexOf(elem) === pos;
        })

        try {
            await guildMember.roles.add(rolesToAddClean)
            await guildMember.setNickname(`${ArchivedUser.firstName} ${ArchivedUser.lastName[0]}.`)
        } catch (error) {
            console.error(error)
        }
    }
}

function getVisitorRoleByName(name) {
    const deVinciGuild = client.guilds.cache.get('731531911465271417')
    const allRoles = deVinciGuild.roles.cache
    return allRoles.find(role => role.name.toLowerCase().includes(name.toLowerCase()) && role.name.includes('☑'))
}

function getStaffRoleByName(name) {
    const deVinciGuild = client.guilds.cache.get('731531911465271417')
    const allRoles = deVinciGuild.roles.cache
    return allRoles.find(role => role.name.toLowerCase().includes(name.toLowerCase()) && !role.name.includes('☑'))
}