const BaseEvent = require('../../utils/structures/BaseEvent')
const mongoose = require('mongoose');

module.exports = class ReactionRemoveEvent extends BaseEvent {
    constructor() {
        super('messageReactionRemove');
    }

    async run (bot, reaction, user) {
        if (user.bot) return;
        if (reaction.message.guild.id != '731531911465271417') return
	    if (reaction.partial) {
	    	try {
	    		await reaction.fetch();
	    	} catch (err) {
	    		console.log('Something went wrong when fetching the message: ', err);
	    		return;
	    	}
        }
        if (reaction.message.channel.parent.id != '753885514158964756') return

        const reactionEmoji = reaction.emoji

        const Asso = await client.reactionAssos.get(reactionEmoji.toString());
        const visitorRole = await getVisitorRoleByName(Asso.name)
        const allMembers = reaction.message.guild.members.cache
        const guildMember = allMembers.get(user.id)

        try {
            await guildMember.roles.remove(visitorRole)
        } catch (error) {
            console.log(error)
        }

        const User = await mongoose.model('User').findOne({ discordId: guildMember.user.id })
        if (User && User.id) {
            const userAssos = User.assosAsVisitor.map(asso => ({
                assoName: asso.assoName,
                userStatus: asso.userStatus,
                userIsBan: asso.userIsBan,
            }));
        
            let existingAssoIndex = userAssos.findIndex(userAsso => userAsso.assoName === Asso.name)
                if (existingAssoIndex >= 0) {
                    userAssos[existingAssoIndex].userIsBan = true
                }
        
            User.assosAsVisitor = userAssos;
    
            try {
                await User.save()
            } catch (err) {
                throw err;
            }
        }

    }
}

function getVisitorRoleByName(name) {
    const deVinciGuild = client.guilds.cache.get('731531911465271417')
    const allRoles = deVinciGuild.roles.cache
    return allRoles.find(role => role.name.toLowerCase().includes(name.toLowerCase()) && role.name.includes('☑'))
}