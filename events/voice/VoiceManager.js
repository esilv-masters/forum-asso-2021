const BaseEvent = require('../../utils/structures/BaseEvent')

module.exports = class VoiceStateUpdate extends BaseEvent {
    constructor() {
        super('voiceStateUpdate');
    }

    async run (bot, oldState, newState) {
        if (newState.channel && newState.channel.name === '☔┃AutoChannel' && newState.guild.id === '731531911465271417') {
            const guild = newState.guild
            if (guild.id != '731531911465271417') return
            const allChannels = guild.channels.cache
            let parameters = {
                name: `🔊┃Vocal de ${newState.member.user.username}`,
                parent: allChannels.get(newState.channel.parent.id),
                type: 'voice',
                permissions: newState.channel.permissionOverwrites
            }
            guild.channels.create(parameters.name, {
                type: parameters.type,
                parent: parameters.parent,
                permissionOverwrites: parameters.permissions
            }).then(async (c) => {
                await newState.setChannel(c)
            })
        }
        if (oldState.channel && oldState.channel.name.includes('┃Vocal de')) {
            if (oldState.channel.members.size === 0) {
                oldState.channel.delete()
            }
        }
    }
}