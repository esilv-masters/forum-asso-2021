const BaseEvent = require('../../utils/structures/BaseEvent');
const Guild = require('../../src/schemas/GuildSchema');
const { showCommandLoad } = require('../../utils/register')
const mongoose = require('mongoose')

module.exports = class ReadyEvent extends BaseEvent {
    constructor() {
        super('ready')
    }

    async run(client) {
        client.user.setActivity('Message me for help !')
        client.admins.set('212357824557219840', true)

        console.log(`Bot ${client.user.username} loaded and ready !`)
        showCommandLoad()

        const guildArray = client.guilds.cache.array();
        for (let i = 0; i < guildArray.length; i++) {
            const guild = guildArray[i];
            const guildConfig = await Guild.findOne({ guildId: guild.id });
            if (guildConfig) {
                client.config.set(guild.id, guildConfig)
                console.log(`Loaded data for guild : ${guild.name}`)
            } else {
                Guild.create({
                    guildId: guild.id,
                    guildName: guild.name
                }, async (err) => {
                    if (err) throw err && console.log(`There was an error trying to save GUILD : ${guild.name} to the database !`)
                    else throw new Error(`⚠️ Guild : ${guild.name} wasn't saved in the database, created new entry ! ⚠️`)
                }) 
            }
        }

        const Assos = await mongoose.model('Asso').find({})
        for (const asso of Assos) {
            client.reactionAssos.set(asso.emoji, asso)
        }

        const Users = await mongoose.model('User').find({})
        for (const user of Users) {
            client.allUsers.set(user.discordId, user)
        }
        console.log(`Cached Users : ${client.allUsers.size}`)
    }
}