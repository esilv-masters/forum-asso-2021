const BaseEvent = require('../../utils/structures/BaseEvent')

module.exports = class channelCreateEvent extends BaseEvent {
    constructor() {
        super('channelCreate')
    }

    async run(client, guildChannel) {
        if (guildChannel.guild && guildChannel.guild.id != '731531911465271417') return
        const allGuildChannels = guildChannel.guild.channels.cache

        if (allGuildChannels.size > 475) return console.log(`WARNING TOO MUCH GUILD CHANNELS : ${allGuildChannels.size}`)
    }
}

const sleep = (ms) => {
    return new Promise((resolve) => {setTimeout(resolve, ms)})
}