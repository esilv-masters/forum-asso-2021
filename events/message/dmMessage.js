const BaseEvent = require('../../utils/structures/BaseEvent');
const Discord = require('discord.js');
const Ticket = require('../../src/schemas/TicketSchema');

const { createTicketTranscript } = require('../../utils/functions/createTicketTranscript')


module.exports = class DmMessageEvent extends BaseEvent {
    constructor() {
        super('message')
    }

    async run(client, message) {
        if (message.author.bot) return;
        if (message.guild) return;

        const existingDBTicket = await Ticket.findOne({ dmChannelId: message.channel.id })
        const homeGuild = client.guilds.cache.get('731531911465271417')
        const ticketsCategory = homeGuild.channels.cache.get('752848266076618783')
        homeGuild.members.fetch()
        const memberOnServer = homeGuild.members.cache.get(message.author.id);
        if (!memberOnServer) return console.log('Member not on server');

        if (!existingDBTicket) {
            try {
                let createdChannel = await createServerChannel(homeGuild, ticketsCategory, message.author.username)
                Ticket.create({
                    dmChannelId: message.channel.id,
                    userId: message.author.id,
                    linkedChannelId: createdChannel.id,
                    guildId: homeGuild.id,
                    name: message.author.username
                }, async (err) => {
                    if (err) throw err;
                    else {
                        message.channel.send(`**✅ | **Interface de contact avec le support ouverte, un modérateur vous répondra sous peu !`)
                        message.channel.send(`**ℹ️ |** *La réaction du bot par 🌐 indique que votre message a bien été transmis !*`)
                        let embed = new Discord.MessageEmbed()
                            .setDescription(`Nouvelle demande d'aide crée par \`${message.author.username}\``)
                            .setColor('#e67e22')
                        
                        createdChannel.send('@everyone',embed)
                        let attachFiles = []
                        message.attachments.each(attachment => attachFiles.push(attachment))
                        createdChannel.send(`\`${message.author.username} :\` ${message.content}`, {
                            files: attachFiles
                        })
                        message.react('🌐')
                    }
                })
            } catch (err) {
                if (err) throw err;
            }
            
        } else {
            const linkedChannel = homeGuild.channels.cache.get(existingDBTicket.linkedChannelId)

            if (linkedChannel) {
                let attachFiles = []
                message.attachments.each(attachment => attachFiles.push(attachment))
                linkedChannel.send(`\`${message.author.username} :\` ${message.content}`, {
                    files: attachFiles
                })
                message.react('🌐')
            } else {
                new Error(`Can't find matching guild channel for ticket ID : ${existingDBTicket.dmChannelId}`)
                message.channel.send(`**❌ | **Je ne suis pas arrivé à joindre le serveur distant, création d'un nouveau canal de communication en cours...`)
                const newChannel = await createServerChannel(homeGuild, ticketsCategory, message.author.username)
                await Ticket.updateOne({ dmChannelId: message.channel.id }, { linkedChannelId: newChannel.id }, {}, async (err, result) => {
                    if (err) throw err;
                    else {
                        message.channel.send(`**✅ | **Canal de communication avec le serveur rouvert, un modérateur vous répondra sous peu !`)
                        message.channel.send(`**ℹ️ |** *La réaction du bot par 🌐 indique que votre message a bien été transmis !*`)
                        let fileName = await createTicketTranscript(client, message.author.username.toLowerCase(), message.channel.id, '752848266076618783')
                        let embed = new Discord.MessageEmbed()
                        .setDescription(`**⚠️ TICKET DEJA EXISTANT ⚠️**\nCe ticket a été réouvert car il a été supprimé manuellement sans avoir été résolu\n\n🔽 Visionner les derniers messages échangés avec l'utilisateur en question 🔽`)
                        .setColor('FF6700')
                        
                        let transcript = new Discord.MessageAttachment(`./files/transcripts/${fileName}`, fileName)
                        await newChannel.send(embed)
                        await newChannel.send(transcript)

                        let attachFiles = []
                        message.attachments.each(attachment => attachFiles.push(attachment))
                        await newChannel.send(`\`${message.author.username} :\` ${message.content}`, {
                            files: attachFiles
                        })
                        message.react('🌐')
                    }
                })
            }
        }

    }
}

async function createServerChannel(guild, categoryChannel, name) {
    return new Promise((resolve) => {
        guild.channels.create(`🎫┃${name}`, {
            type: 'text',
            topic: `${client.config.get(guild.id).prefix}msg <message> pour répondre !`,
            parent: categoryChannel,
            permissionOverwrites: [
                {
                    id: guild.roles.everyone.id,
                    deny: ['VIEW_CHANNEL']
                },
                {
                    id: '753699877766037504',
                    allow: ['VIEW_CHANNEL']
                },
                {
                    id: '731539610605453405',
                    allow: ['VIEW_CHANNEL']
                }
            ]
        }).then(createdChannel => {
            resolve(createdChannel)
        })
    })
}