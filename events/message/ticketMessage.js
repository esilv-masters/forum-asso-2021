const BaseEvent = require('../../utils/structures/BaseEvent');
const Ticket = require('../../src/schemas/TicketSchema');

const createTranscript = require('../../utils/functions/createTicketTranscript')

module.exports = class DmMessageEvent extends BaseEvent {
    constructor() {
        super('message')
    }

    async run (client, message) {
        if (message.guild) return;
        const existingDBTicket = await Ticket.findOne({ dmChannelId: message.channel.id });
        if (existingDBTicket) {
            Ticket.updateOne( { dmChannelId: message.channel.id }, { $push: {messages: {
                authorId: message.author.id,
                authorTag: message.author.tag,
                authorAvatarURL: message.author.displayAvatarURL(),
                createdAt: message.createdAt.toDateString(),
                content: message.content
            }}}, {}, async (err, result) => {
                if (err) throw err;
            })
        }
    }
}