const bodyParser = require('body-parser');
require('dotenv').config();

async function startExpress(app, PORT) {
    const routes = require('../backend/routes');
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(bodyParser.json());
    
    app.use(function(req, res, next) {
        res.setHeader('Access-Control-Allow-Origin', process.env.APP_ORIGIN);
        res.setHeader('Access-Control-Allow-Methods', 'GET, PATCH, POST, DELETE');
        res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
        req.connection.setTimeout(24 * 60 * 60 * 1000);
        next();
    });
    app.listen( PORT, () => console.log(`Running on PORT : ${PORT}`) );

    app.use('/api', routes);
}

module.exports = {
    startExpress
};