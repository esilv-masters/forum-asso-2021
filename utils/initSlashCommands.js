

async function getApp(client, guildId) {
    const app = client.api.applications
    if (guildId) {
        app.guilds(guildId)
    }

    return app
}