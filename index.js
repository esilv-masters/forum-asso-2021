require("dotenv").config()
const Discord = require("discord.js");
global.client = new Discord.Client({ partials: ['MESSAGE', 'CHANNEL', 'REACTION'] });
const mongoose =  require('mongoose');
const express = require('express');
const PORT = process.env.PORT || 8080;

var app = express();

mongoose.connect(process.env.MONGODB_URI, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
  tls: true,
  tlsCAFile: 'ca-certificate.crt'
}).then(connection => {
  console.log(`Connected to MongoDB`)
}).catch(err => {
  if (err) throw err;
})

const { 
    registerCommands, 
    registerEvents
  } = require('./utils/register');

const { 
  startExpress
} = require('./utils/startExpress');


(async () => {
  client.commands = new Map();
  client.config = new Map();
  client.aliases = new Map();
  client.admins = new Map();
  client.reactionAssos = new Map();
  client.allUsers = new Map();
  await registerCommands(client, '../commands');  
  await registerEvents(client, '../events');
  await startExpress(app, PORT)
  await client.login(process.env.DISCORD_BOT_TOKEN);
})();

