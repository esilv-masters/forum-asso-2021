const checkJwt = require('../../utils/checkJwt');
const router = require('express').Router();
const mongoose = require('mongoose');

const {
    getEmoji,
    removeEmojis
} = require('../../utils/functions/utilitaryFunctions');

//ENDPOINT GET ALL ASSOS ✅
router.get('/', checkJwt, (req, res) => {
    mongoose.model('Asso').find({}, (err, data) => {
        if (err){
            res.send(err);
        } else {
            res.send(data);
        }
    });
});

//ENDPOINT GET ALL CHANNELS FOR ASSO ✅
router.get('/:id/channels', checkJwt, async (req, res) => {
    const staffId = req.user.sub.split('|')[2];
    const staffUser = await getGuildUserById(staffId)
    const assoId = req.params.id

    const Asso = await mongoose.model('Asso').findById(assoId).catch(err => res.status('400').send(err));

    if (Asso.id) {
        if (staffUser.hasPermission('BAN_MEMBERS') || staffUser.id === Asso.referentId) {
            let assoCategoryChannel = await findChannelByName(Asso.name, 'category')
            if (assoCategoryChannel) {
                let childChannels = await findChildChannelsForCategory(assoCategoryChannel)
                if (childChannels) {
                    let arrayOfChannels = []
                    for (const [key, channel] of childChannels.entries()) {
                        let permissionsToAdd = []
                        for (const [id, value] of channel.permissionOverwrites.entries()) {
                            let role = getRoleById(id)
                            if (role && role.name.includes(Asso.name)) {
                                if (role.name.includes('☑')) {
                                    await permissionsToAdd.push(
                                        {
                                            id: 'visitorRoleId',
                                            allow: value.allow.toArray(),
                                            deny: value.deny.toArray()
                                        }
                                    )
                                }
                            }
                        }
                        await arrayOfChannels.push({
                            fullname: channel.name,
                            emoji: getEmoji(channel.name),
                            name: channel.name.split('┃')[1],
                            type: channel.type,
                            deleted: channel.deleted,
                            id: channel.id,
                            rawPosition: channel.rawPosition,
                            parentID: channel.parentId,
                            permissionOverwrites: permissionsToAdd,
                            topic: channel.topic,
                            nsfw: channel.nsfw,
                            createdTimestamp: channel.createdTimestamp
                        })
                    }
                    await res.send(arrayOfChannels);
                } else res.status('204').send()
            } else res.status('204').send()
        } else res.status('403').send()
    } else res.status('400').send()
});

//ENDPOINT EDIT CHANNEL IN ASSO CATEGORY ✅
router.patch('/:assoId/channels/:channelId', checkJwt, async (req, res) => {
    const staffId = req.user.sub.split('|')[2];
    const staffUser = await getGuildUserById(staffId)
    const channelId = req.params.channelId
    const assoId = req.params.assoId

    const Asso = await mongoose.model('Asso').findById(assoId).catch(err => res.status('400').send(err));

    if (Asso.id) {
        if (staffUser.hasPermission('BAN_MEMBERS') || staffUser.id === Asso.referentId) {
            let assoCategoryChannel = await findChannelByName(Asso.name, 'category')
            if (assoCategoryChannel) {
                let channelToModify = await findChannelInCategoryById(channelId, assoCategoryChannel)
                if (channelToModify) {
                    let permissionOverwrites = req.body.permissionOverwrites
                    let visitorRole = await getVisitorRoleByName(Asso.name)

                    let permissionOverwritesArray = channelToModify.permissionOverwrites.filter(perm => perm.id != Asso.linkedRoleId && perm.id != visitorRole.id).toJSON()
    
                    for (let i = 0; i < permissionOverwrites.length; i++) {
                        let permission = permissionOverwrites[i];
                
                        if (permission.id === "visitorRoleId") {
                            permissionOverwrites[i].id = visitorRole.id
                        }
                        permissionOverwritesArray.push(permissionOverwrites[i])
                    }

                    if (channelToModify.name === `${req.body.emoji}┃${req.body.name}`) {
                        await channelToModify.edit({
                            permissionOverwrites: permissionOverwritesArray
                        }).then(updatedChannel => {
                            res.send(updatedChannel)
                        }).catch(err => {
                            res.status('500').send(err);
                        }) 
                    } else {
                        await channelToModify.edit({
                            name: `${req.body.emoji}┃${req.body.name}`,
                            permissionOverwrites: permissionOverwritesArray
                        }).then(updatedChannel => {
                            res.send(updatedChannel)
                        }).catch(err => {
                            res.status('500').send(err);
                        })
                    }
                } else res.status('410').send()
            } else res.status('204').send()
        } else res.status('403').send()
    } else res.status('400').send()
});

//ENDPOINT CREATE CHANNEL IN ASSO CATEGORY -- ✅
router.post('/:assoId/channels', checkJwt, async (req, res) => {
    const staffId = req.user.sub.split('|')[2];
    const staffUser = await getGuildUserById(staffId)
    const assoId = req.params.assoId

    const Asso = await mongoose.model('Asso').findById(assoId).catch(err => res.status('400').send(err));

    if (Asso.id) {
        if (staffUser.hasPermission('BAN_MEMBERS') || staffUser.id === Asso.referentId) {
            let assoCategoryChannel = await findChannelByName(Asso.name, 'category')
            if (assoCategoryChannel) {
                let permissionOverwrites = req.body.permissionOverwrites
                let visitorRole = await getVisitorRoleByName(Asso.name)

                for (let i = 0; i < permissionOverwrites.length; i++) {
                    let permission = permissionOverwrites[i];
            
                    if (permission.id === "visitorRoleId") {
                        permissionOverwrites[i].id = visitorRole.id
                    }
                }

                await permissionOverwrites.push({
                    id: assoCategoryChannel.guild.roles.everyone.id,
                    deny: ["VIEW_CHANNEL", "SEND_MESSAGES", "CONNECT", "MOVE_MEMBERS", "SPEAK", "MANAGE_MESSAGES"]
                }, {
                    id: Asso.linkedRoleId,
                    allow: ["VIEW_CHANNEL", "SEND_MESSAGES", "CONNECT", "MOVE_MEMBERS", "SPEAK", "MANAGE_MESSAGES"]
                }, {
                    id: '731539610605453405',
                    allow: ['VIEW_CHANNEL', 'SEND_MESSAGES', 'CONNECT', "MOVE_MEMBERS", "SPEAK", "MANAGE_MESSAGES"]
                }, {
                    id: '753699877766037504',
                    allow: ['VIEW_CHANNEL', 'SEND_MESSAGES', 'CONNECT', "MOVE_MEMBERS", "SPEAK", "MANAGE_MESSAGES"]
                })

                const deVinciGuild = await getDeVinciGuild()
                await deVinciGuild.channels.create(req.body.name, {
                    type: req.body.type,
                    parent: assoCategoryChannel,
                    permissionOverwrites: permissionOverwrites
                }).then(newChannel => {
                    res.send(newChannel);
                }).catch(err => {
                    res.status('500').send(err);
                })
            } else res.status('204').send()
        } else res.status('403').send()
    } else res.status('400').send()
});

//ENDPOINT DELETE CHANNEL IN ASSO CATEGORY -- TOCHECK
router.delete('/:assoId/channels/:channelId', checkJwt, async (req, res) => {
    const staffId = req.user.sub.split('|')[2];
    const staffUser = await getGuildUserById(staffId)
    const channelId = req.params.channelId
    const assoId = req.params.assoId
    const comment = req.body.comment

    const Asso = await mongoose.model('Asso').findById(assoId).catch(err => res.status('400').send(err));

    if (Asso.id) {
        if (staffUser.hasPermission('BAN_MEMBERS') || staffUser.id === Asso.referentId) {
            let assoCategoryChannel = await findChannelByName(Asso.name, 'category')
            if (assoCategoryChannel) {
                let channelToDelete = await findChannelInCategoryById(channelId, assoCategoryChannel)
                if (channelToDelete) {
                    channelToDelete.delete(comment).then(deletedChannel => {
                        res.send(deletedChannel)
                    }).catch(err => {
                        res.status('500').send(err);
                    })
                } else res.status('410').send()
            } else res.status('204').send()
        } else res.status('403').send()
    } else res.status('400').send()
});

//ENDPOINT CHANGE STAFF STATUS USER ASSO ✅
router.patch('/:assoId/staffs/:userId', checkJwt, async (req, res) => {
    const staffId = req.user.sub.split('|')[2];
    const staffUser = await getGuildUserById(staffId)
    const userId = req.params.userId
    const assoId = req.params.assoId
    const targetUser = await getGuildUserById(userId)

    const allRoles = getDeVinciGuildRoles()

    const newStatus = req.body.status

    const Asso = await mongoose.model('Asso').findById(assoId).catch(err => res.status('400').send(err));

    const User = await mongoose.model('User').findOne({ discordId: userId }).catch(err => res.status('400').send(err));

    if (Asso.id && User.id) {
        if (staffUser.hasPermission('BAN_MEMBERS') || staffUser.id === Asso.referentId) {
            let staffRole = getRoleById(Asso.linkedRoleId)
            let UserAssosAsStaffArray = User.assosAsStaff

            let assoInUserIndex = await UserAssosAsStaffArray.findIndex(asso => asso.assoName === Asso.name)
            if (assoInUserIndex >= 0) {
                try {
                    User.assosAsStaff[assoInUserIndex].userStatus = newStatus;
                    await User.save()
                    res.send({
                        operation: 'update',
                        success: true,
                        newStatus: newStatus
                    })
                } catch (err) {
                    console.error(err)
                    res.status('500').send()
                }
            } else {
                try {
                    User.assosAsStaff.push({
                        assoName: Asso.name,
                        userStatus: newStatus,
                        userIsBan: false
                    })
                    await User.save()
                    res.send({
                        operation: 'create',
                        success: true,
                        newStatus: newStatus
                    })
                } catch (err) {
                    console.error(err)
                    res.status('500').send()
                }
            }

            const dividerLowAssoRoles = allRoles.get('806846373827575828').rawPosition
            const dividerHighAssoRoles = allRoles.get('806192779230707772').rawPosition
            let targetUserStaffRoles = await targetUser.roles.cache.filter(r => r.rawPosition > dividerLowAssoRoles && r.rawPosition < dividerHighAssoRoles && !r.name.includes('▬▬'))

            switch(newStatus) {
                case 'accepted':
                    await targetUser.roles.add([staffRole.id, '881553681353420821'])
                    break;

                case 'refused':
                    await targetUser.roles.remove(staffRole)
                    if (targetUserStaffRoles.size === 1) {
                        await targetUser.roles.remove('881553681353420821')
                    }
                    break;
                
                default: 
                    break;
            }
        } else res.status('403').send()
    } else res.status('400').send()
});

//ENDPOINT CHANGE STATUS VISITOR USER ASSO ✅
router.patch('/:assoId/visitors/:userId', checkJwt, async (req, res) => {
    const staffId = req.user.sub.split('|')[2];
    const staffUser = await getGuildUserById(staffId)
    const userId = req.params.userId
    const assoId = req.params.assoId
    const targetUser = await getGuildUserById(userId)

    const newStatus = req.body.status

    const Asso = await mongoose.model('Asso').findById(assoId).catch(err => res.status('400').send(err));

    const User = await mongoose.model('User').findOne({ discordId: userId }).catch(err => res.status('400').send(err));

    if (Asso.id && User.id) {
        if (staffUser.hasPermission('BAN_MEMBERS') || staffUser.id === Asso.referentId) {
            const visitorRole = await getVisitorRoleByName(Asso.name)
            let UserAssosAsVisitorArray = User.assosAsVisitor
            let assoInUserIndex = await UserAssosAsVisitorArray.findIndex(asso => asso.assoName === Asso.name)
            if (assoInUserIndex >= 0) {
                try {
                    User.assosAsVisitor[assoInUserIndex].userStatus = newStatus
                    User.save()
                    res.send({
                        operation: 'update',
                        success: true,
                        newStatus: newStatus
                    })
                } catch (err) {
                    console.error(err)
                    res.status('500').send()
                }
            } else {
                try {
                    User.assosAsVisitor.push({
                        assoName: Asso.name,
                        userStatus: newStatus,
                        userIsBan: false
                    })
                    User.save()
                    res.send({
                        operation: 'create',
                        success: true,
                        newStatus: newStatus
                    })
                } catch (err) {
                    console.error(err)
                    res.status('500').send()
                }
            }

            switch(newStatus) {
                case 'accepted':
                    targetUser.roles.add(visitorRole)
                    break;

                case 'refused':
                    targetUser.roles.remove(visitorRole)
                    break;
                
                default: 
                    break;
            }
        } else res.status('403').send()
    } else res.status('400').send()
});

router.patch('/:assoId/visitors/:userId/ban', checkJwt, async (req, res) => {
    const staffId = req.user.sub.split('|')[2];
    const staffUser = await getGuildUserById(staffId)
    const userId = req.params.userId
    const assoId = req.params.assoId
    const targetUser = await getGuildUserById(userId)

    const banStatus = req.body.isBan

    const Asso = await mongoose.model('Asso').findById(assoId).catch(err => res.status('400').send(err));

    const User = await mongoose.model('User').findOne({ discordId: userId }).catch(err => res.status('400').send(err));

    if (Asso.id && User.id) {
        if (staffUser.hasPermission('BAN_MEMBERS') || staffUser.id === Asso.referentId) {
            const visitorRole = await getVisitorRoleByName(Asso.name)
            let UserAssosAsVisitorArray = User.assosAsVisitor
            let assoInUserIndex = await UserAssosAsVisitorArray.findIndex(asso => asso.assoName === Asso.name)

            if (assoInUserIndex >= 0) {
                try {
                    User.assosAsVisitor[assoInUserIndex].userIsBan = banStatus
                    User.save()
                    res.send({
                        operation: 'ban',
                        success: true,
                        newStatus: banStatus
                    })
                } catch (err) {
                    console.error(err)
                    res.status('500').send()
                }
                if (banStatus === true) targetUser.roles.remove(visitorRole)
            } else res.status('204').send()
        }
    }
})


module.exports = router;

function findChannelByName(name, type) {
    const deVinciGuild = client.guilds.cache.get('731531911465271417')
    const allChannels = deVinciGuild.channels.cache
    return allChannels.find(channel => channel.name.toLowerCase().includes(name.toLowerCase()) && channel.type === type);
}

function findChannelsByName(name, type) {
    const deVinciGuild = client.guilds.cache.get('731531911465271417')
    const allChannels = deVinciGuild.channels.cache
    return allChannels.filter(channel => channel.name.toLowerCase().includes(name.toLowerCase()) && channel.type === type);
}

function findChannelInCategoryById(channelId, parent) {
    const deVinciGuild = client.guilds.cache.get('731531911465271417')
    const allChannels = deVinciGuild.channels.cache
    return allChannels.find(channel => channel.id === channelId && channel.parent === parent);
}

function findChildChannelsForCategory(category) {
    const deVinciGuild = client.guilds.cache.get('731531911465271417')
    const allChannels = deVinciGuild.channels.cache
    return allChannels.filter(channel =>  channel.parent === category);
}

function getGuildUserById(userId) {
    const deVinciGuild = client.guilds.cache.get('731531911465271417')
    return deVinciGuild.members.fetch(userId)
}

function getDeVinciGuildMembers() {
    const deVinciGuild = client.guilds.cache.get('731531911465271417')
    return deVinciGuild.members.cache
}

function getDeVinciGuildRoles() {
    const deVinciGuild = client.guilds.cache.get('731531911465271417')
    return deVinciGuild.roles.cache
}

function getDeVinciGuildChannels() {
    const deVinciGuild = client.guilds.cache.get('731531911465271417')
    return deVinciGuild.channels.cache
}

function getRoleByName(name) {
    const deVinciGuild = client.guilds.cache.get('731531911465271417')
    const allRoles = deVinciGuild.roles.cache
    return allRoles.find(role => role.name.toLowerCase().includes(name.toLowerCase()))
}

function getRoleById(id) {
    const deVinciGuild = client.guilds.cache.get('731531911465271417')
    const allRoles = deVinciGuild.roles.cache
    return allRoles.get(id)
}

function getDeVinciGuild() {
    return client.guilds.cache.get('731531911465271417')
}

function getVisitorRoleByName(name) {
    const deVinciGuild = client.guilds.cache.get('731531911465271417')
    const allRoles = deVinciGuild.roles.cache
    return allRoles.find(role => role.name.toLowerCase().includes(name.toLowerCase()) && role.name.includes('☑'))
}