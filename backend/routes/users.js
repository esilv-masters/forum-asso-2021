const checkJwt = require('../../utils/checkJwt');
const router = require('express').Router();
const mongoose = require('mongoose');
const PreRegistredUser = require('../../src/schemas/PreRegistredUserSchema')

// ENDOPOINT GET USERS ✅
router.get('/', checkJwt, (req, res) => {
    mongoose.model('User').find({}, (err, data) => {
        if (err){
            res.send(err);
        } else {
            res.send(data);
        }
    });
});

// ENDOPOINT GET USER BY DISCORDID ✅
router.get('/:id', checkJwt, async (req, res) => {
    const User = await mongoose.model('User').findOne({ discordId: req.params.id }).catch(err => res.status('500').send(err))
    const existingPreRegistredUser = await PreRegistredUser.findOne({ discordId: req.params.id }).catch(err => res.status('500').send(err))

    if (User && User.id) res.send(User);
    else if (existingPreRegistredUser && existingPreRegistredUser.id) res.send(existingPreRegistredUser);
    else res.send(null);
});

// ENDOPOINT PATCH USER ✅
router.patch('/:id', checkJwt, async (req, res) => {
    const body = req.body;
    const User =  client.allUsers.get(req.params.id)
    const existingPreRegistredUser = await PreRegistredUser.findOne({ discordId: req.params.id }).catch(err => console.log(err))
    const ArchivedUser = await mongoose.model('UserArchive').findOne({ discordId: req.params.id }).catch(err => console.log(err))

    const guildMember = await getGuildUserById(req.params.id)

    if (!guildMember) {
        try {
            guildMember = await fetchGuildUser(req.params.id)
        } catch (error) {
            console.error(`${req.params.id} not on guild`)
        }
    }

    if (guildMember && User && User.id) {

        User.assosAsVisitor = body.assosAsVisitor ? body.assosAsVisitor.map(asso => ({
            assoName: asso,
            userStatus: 'accepted',
            userIsBan: false
        })) : User.assosAsVisitor
        User.assosAsStaff = body.assosAsStaff ? body.assosAsStaff.map(asso => ({
            assoName: asso, 
            userStatus: 'pending',
            userIsBan: false
        })) : User.assosAsStaff
        User.schoolYear = body.schoolYear
        User.firstName = body.firstName
        User.lastName = body.lastName.toUpperCase()
        User.school = body.school


        let allRoles = await getDeVinciGuildRoles()
        let schoolRole = await allRoles.find(r => r.name.includes(`${body.school} - A${body.schoolYear}`))
        let rolesToAdd = []
        for (let i = 0; i < User.assosAsVisitor.length; i++) {
            let assoAsVisitor = User.assosAsVisitor[i];
            let visitorRole = await getVisitorRoleByName(`${assoAsVisitor.assoName}`)
            if (visitorRole) await rolesToAdd.push(visitorRole.id)
        }
        if (User.isReferent === true) {
            let ReferentAsso = await mongoose.model('Asso').findOne({ referentId: User.discordId })
            await rolesToAdd.push(ReferentAsso.linkedRoleId)
        }

        const rolesToRemove = getInitiationRoles(guildMember)

        if (rolesToRemove && rolesToRemove.size > 0) await guildMember.roles.remove(rolesToRemove)

        if (schoolRole) {
            rolesToAdd.push(schoolRole.id, '731532207772139521')
            await guildMember.roles.add(rolesToAdd)
            try {
                await guildMember.setNickname(`${body.firstName} ${body.lastName[0].toUpperCase()}.`)
            } catch (error) {
                console.log(error)
            }
        }
        try {
            await mongoose.model('User').updateOne({discordId: User.discordId}, {
                firstName: User.firstName,
                lastName: User.lastName,
                school: User.school,
                schoolYear: User.schoolYear,
                assosAsVisitor: User.assosAsVisitor,
                assosAsStaff: User.assosAsStaff
            })
            client.allUsers.delete(req.params.id)
            client.allUsers.set(req.params.id, User)
            res.send(User);
        } catch (err) {
            res.status('500').send(err);
        }
    } else if (!guildMember && !(existingPreRegistredUser && existingPreRegistredUser.id)) {
        const newUser = await PreRegistredUser.create({
            discordId: req.params.id,
            firstName: body.firstName,
            lastName: body.lastName.toUpperCase(),
            school: body.school,
            schoolYear: body.schoolYear,
            assosAsVisitor: body.assosAsVisitor.map(asso => ({
                assoName: asso,
                userStatus: 'accepted',
                userIsBan: false
            })),
            assosAsStaff: body.assosAsStaff.map(asso => ({
                assoName: asso, 
                userStatus: 'pending',
                userIsBan: false
            }))
        })
        if (ArchivedUser && ArchivedUser.id) {
            newUser.avatarURL = ArchivedUser.avatarURL
            newUser.username = ArchivedUser.username
            newUser.save()
        }
        res.send(newUser);

    } else if (!guildMember && existingPreRegistredUser && existingPreRegistredUser.id) {
        if (ArchivedUser && ArchivedUser.id) {
            existingPreRegistredUser.avatarURL = ArchivedUser.avatarURL
            existingPreRegistredUser.username = ArchivedUser.username
        }
        const resUser = await PreRegistredUser.updateOne({ discordId: req.params.id }, {
            firstName: body.firstName,
            lastName: body.lastName.toUpperCase(),
            school: body.school,
            schoolYear: body.schoolYear,
            assosAsVisitor: body.assosAsVisitor.map(asso => ({
                assoName: asso,
                userStatus: 'accepted',
                userIsBan: false
            })),
            assosAsStaff: body.assosAsStaff.map(asso => ({
                assoName: asso, 
                userStatus: 'pending',
                userIsBan: false
            }))
        })
        res.send(resUser)
    } else {
        res.status('500').send();
    }
});

// ENDOPOINT PATCH USER ASSOS AS STAFF ✅
router.patch('/:id/assosAsStaff', checkJwt, async (req, res) => {

    const User = await mongoose.model('User').findOne({ discordId: req.params.id }).catch(err => res.status('400').send(err))

    if (User && User.id) {
        const userAssos = User.assosAsStaff.map(asso => ({
            assoName: asso.assoName,
            userStatus: asso.userStatus,
            userIsBan: asso.userIsBan,
        }));
    
        let assosToAdd = req.body;
        for (const asso of assosToAdd) {
            let existingAssoIndex = userAssos.findIndex(userAsso => userAsso.assoName === asso)
            if (existingAssoIndex >= 0) {
                userAssos[existingAssoIndex].userIsBan = false
            } else {
                userAssos.push({
                    assoName: asso,
                    userStatus: 'pending',
                    userIsBan: false
                })
            }
        }
    
        User.assosAsStaff = userAssos;

        try {
            await User.save()
            res.send(User);
        } catch (err) {
            res.status('500').send(err);
        }
    } else res.status('400').send()
    
});

// ENDOPOINT PATCH USER ASSOS AS VISITOR ✅
router.patch('/:id/assosAsVisitor', checkJwt, async (req, res) => {

    const User = await mongoose.model('User').findOne({ discordId: req.params.id }).catch(err => res.status('400').send(err))
    const guildMember = await getGuildUserById(req.params.id)

    if (User && User.id && guildMember) {
        const userAssos = User.assosAsVisitor.map(asso => ({
            assoName: asso.assoName,
            userStatus: asso.userStatus,
            userIsBan: asso.userIsBan,
        }));
    
        let assosToAdd = req.body;
        let rolesToAdd = []
        for (const asso of assosToAdd) {
            let existingAssoIndex = userAssos.findIndex(userAsso => userAsso.assoName === asso)
            if (existingAssoIndex >= 0) {
                userAssos[existingAssoIndex].userIsBan = false
            } else {
                userAssos.push({
                    assoName: asso,
                    userStatus: 'accepted',
                    userIsBan: false
                })
            }
            let visitorRole = getVisitorRoleByName(asso)
            rolesToAdd.push(visitorRole.id)
        }
    
        User.assosAsVisitor = userAssos;

        try {
            await User.save()
            await guildMember.roles.add(rolesToAdd)
            res.send(User);
        } catch (err) {
            res.status('500').send(err);
        }
    } else res.status('400').send()
    
});

// ENDOPOINT KICK USER ✅
router.post('/:id/kick', checkJwt, async (req, res) => {
    const staffId = req.user.sub.split('|')[2];
    const targetId = req.params.id;
    const comment = req.body.comment;

    let staffUser = await getGuildUserById(staffId);
    let userToKick = await getGuildUserById(targetId);

    if (staffUser.hasPermission('KICK_MEMBERS')) {
        if (!userToKick) return res.status('204').send();
        if (userToKick.kickable) {
            userToKick.kick(comment).then(kickedMember => {
                res.send({
                    operation: 'kick',
                    success: true,
                    message: `${userToKick.user.username} a été expulsé avec succès !`,
                    user: kickedMember
                });
            })
        } else res.status('403').send()
    } else res.status('403').send()
});

// ENDOPOINT BAN USER ✅
router.post('/:id/ban', checkJwt, async (req, res) => {
    const staffId = req.user.sub.split('|')[2];
    const targetId = req.params.id;
    const comment = req.body.comment;

    let staffUser = await getGuildUserById(staffId);
    let userToBan = await getGuildUserById(targetId);

    if (staffUser.hasPermission('BAN_MEMBERS')) {
        if (!userToBan) return res.status('204').send();
        if (userToBan.bannable) {
            userToBan.ban({
                reason: comment
            }).then(bannedMember => {
                res.send({
                    operation: 'ban',
                    success: true,
                    message: `${userToKick.user.username} a été banni avec succès !`,
                    user: bannedMember
                });
            })
            res.send(`${userToBan.user.tag} has been banned with success !`)
        } else res.status('403').send()
    } else res.status('403').send()
}); 

module.exports = router;

async function getGuildUserById(userId) {
    const deVinciGuild = await client.guilds.cache.get('731531911465271417')
    return deVinciGuild.members.cache.get(userId)
}

async function fetchGuildUser(userId) {
    const deVinciGuild = await client.guilds.cache.get('731531911465271417')
    return deVinciGuild.members.fetch(userId)
        .catch(err => console.log('User not on guild'))
}

function getDeVinciGuildMembers() {
    const deVinciGuild = client.guilds.cache.get('731531911465271417')
    return deVinciGuild.members.cache
}

async function getDeVinciGuildRoles() {
    const deVinciGuild = await client.guilds.cache.get('731531911465271417')
    return deVinciGuild.roles.cache
}

function getRoleByName(name) {
    const deVinciGuild = client.guilds.cache.get('731531911465271417')
    const allRoles = deVinciGuild.roles.cache
    return allRoles.find(role => role.name.toLowerCase().includes(name.toLowerCase()))
}

function getVisitorRoleByName(name) {
    const deVinciGuild = client.guilds.cache.get('731531911465271417')
    const allRoles = deVinciGuild.roles.cache
    return allRoles.find(role => role.name.toLowerCase().includes(name.toLowerCase()) && role.name.includes('☑'))
}

async function getInitiationRoles(guildMember) {
    const allRoles = await getDeVinciGuildRoles()
    let roles = guildMember.roles

    return roles.cache.filter(role => role.rawPosition < allRoles.get('736982718574887003').rawPosition && role.rawPosition > allRoles.get('753030184751333376').rawPosition || role.rawPosition < allRoles.get('736982911735300138').rawPosition && role.rawPosition > allRoles.get('806846373827575828').rawPosition || role.id === '731532207772139521')
}