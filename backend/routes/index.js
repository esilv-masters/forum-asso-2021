const router = require('express').Router();
const assos = require('./assos');
const users = require('./users');

router.use('/assos', assos)
router.use('/users', users)

module.exports = router;