const mongoose =  require('mongoose');

const AssoSchema = new mongoose.Schema({
    assoName: {
        type: String,
        required: true
    },
    userStatus: String,
    userIsBan: Boolean,
});

const UserSchema = new mongoose.Schema({
    firstName: String,
    lastName: String,
    discordId: {
        type: String,
        required: true,
        unique: true
    },
    school: String,
    schoolYear: Number,
    assosAsStaff: [AssoSchema],
    assosAsVisitor: [AssoSchema],
});

module.exports = mongoose.model('PreRegistredUser', UserSchema);