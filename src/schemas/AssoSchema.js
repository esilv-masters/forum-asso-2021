const mongoose = require('mongoose');

const AssoSchema = new mongoose.Schema({
    linkedRoleId: {
        type: String,
        unique: true,
        required: true
    },
    linkedCategoryId: String,
    name: {
        type: String,
        required: true
    },
    emoji: String,
    color: String,
    category: String,
    referentId: String,
    referentName: String,
    description_fr: String,
    description_en: String
});

module.exports = mongoose.model('Asso', AssoSchema);