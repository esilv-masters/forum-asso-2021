const mongoose = require('mongoose');

const MessageSchema = new mongoose.Schema({
    authorId: String,
    authorTag: String,
    authorAvatarURL: String,
    createdAt: String,
    content: String,
});

const TicketSchema = new mongoose.Schema({
    dmChannelId: {
        type: String,
        required: true,
        unique: true
    },
    name: {
        type: String,
        required: true
    },
    userId: {
        type: String,
        required: true
    },
    guildId: {
        type: String,
        required: true
    },
    linkedChannelId: {
        type: String,
        required: true
    },
    claimedByUserId: {
        type: String
    },
    messages: [MessageSchema],
});

module.exports = mongoose.model('Ticket', TicketSchema);