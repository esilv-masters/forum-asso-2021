const mongoose = require('mongoose');

const MessageSchema = new mongoose.Schema({
    authorId: String,
    authorTag: String,
    authorAvatarURL: String,
    createdAt: String,
    content: String,
});

const TicketArchiveSchema = new mongoose.Schema({
    dmChannelId: {
        type: String,
        required: true,
    },
    name: {
        type: String,
        required: true
    },
    userId: {
        type: String,
        required: true
    },
    guildId: {
        type: String,
        required: true
    },
    claimedByUserId: {
        type: String
    },
    messages: [MessageSchema],
    topic: String
});

module.exports = mongoose.model('TicketArchive', TicketArchiveSchema);